import React, { useEffect, useState } from "react";
import axios from "axios";
import CookieService from "../service/CookieService";

const Comics = (props) => {
    const [data, setData] = useState([]);
   
    useEffect(() => {
        comics('');
    }, []);

    const search = (e)=>{
        comics(e.target.value)
       
    }

    const comics = (searchInput) => {
        axios
            .post(
                "/api/comics",
                { searchInput: searchInput },
                {
                    headers: {
                        Authorization:
                            "Bearer " + CookieService.get("access_token"),
                    },
                }
            )
            .then((res) => {
                setData(res.data.data);
            })
            .catch((e) => {
                // alert("Something went wrong, contact your support team");
            });
    };

    return (
        <section className="dishes">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="aligncenter">
                            <h2 className="aligncenter">Marvel's Comics</h2>
                            Marvel Comics, American media and entertainment
                            company that was widely regarded as one of the “big
                            two” publishers in the comic industry.
                            
                        </div>
                        <br />
                        <center>
                        <br />
                            <div class="input-group mb-3">
                                <input
                                    type="text"
                                    class="form-control"
                                    placeholder="Search"
                                    aria-label="search"
                                    aria-describedby="basic-addon1"
                                    onChange={search}
                                />
                            </div>
                        </center>
                    </div>
                </div>

                <div className="row service-v1 margin-bottom-40">
                    {data.map((item, key) => (
                        <div className="col-md-4 md-margin-bottom-40" key={key}>
                            <div className="card small">
                                <div className="card-image">
                                    <img
                                        className="img-responsives"
                                        src={item.thumbnail}
                                        alt=""
                                        width="100%"
                                        height="227px"
                                    />
                                    <span className="card-title">
                                        {item.title}
                                    </span>
                                </div>
                                <div className="card-content">
                                    <p>{item.description}</p>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    );
};

export default Comics;
