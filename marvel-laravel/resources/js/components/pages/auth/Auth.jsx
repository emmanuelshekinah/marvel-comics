import React, { useState } from "react";
import "./app.css";
import axios from "axios";
import CookieService from "../../service/CookieService";
import {
   Link
} from "react-router-dom";
// const expiresAt = 60 * 24;

const Auth = (props) => {

    const [isLoginSession, setIsLoginSession] = useState(true);
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);


    const handleSubmit =(e)=>{
      e.preventDefault()
      axios.post('/api/login',{username: email, password: password}, {headers: {'Content-Type': 'application/json'}})
      .then(res=>{
         console.log(res.data.access_token)

         // date.setTime(date.getTime() + expiresAt * 60 * 1000);
         const options = { path: "/" };
         CookieService.set("access_token", res.data.access_token, options);
         document.getElementById('loggedin').click()
         window.location.reload(true)

      }).catch(e=>{
         alert("Access Denied")
      })
    }

    return (
        <React.Fragment>
            <br />
            <br />
            <br />
            <Link id="loggedin" to='/' ></Link>
            <section class="login-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 login-sec">
                            <h2 class="text-center">
                                {isLoginSession === true
                                    ? "Sign In Now"
                                    : "Sign up Now"}
                            </h2>

                            <form class="login100-form validate-form" onSubmit={handleSubmit}>
                                <div
                                    class={`validate-input ${
                                        isLoginSession === true
                                            ? "d-none"
                                            : "d-block"
                                    }`}
                                >
                                    <span class="label-input100">
                                        Full Name
                                    </span>
                                    <input
                                        class="input100"
                                        type="text"
                                        name="username"
                                        placeholder="Type your username"
                                    />
                                </div>
                                <div class=" validate-input">
                                    <span class="label-input100">Email</span>
                                    <input
                                        class="input100"
                                        type="text"
                                        name="email"
                                        required={true}
                                        defaultValue={email}
                                        onChange={(e)=>{setEmail(e.target.value)}}
                                        placeholder="Type your username"
                                    />
                                </div>

                                <div class=" validate-input">
                                    <span class="label-input100">Password</span>
                                    <input
                                        class="input100"
                                        type="password"
                                        name="password"
                                        required={true}
                                        defaultValue={password}
                                        onChange={(e)=>{setPassword(e.target.value)}}
                                        placeholder="Type your password"
                                    />
                                </div>
                                <div
                                    class={`validate-input ${
                                        isLoginSession === true
                                            ? "d-none"
                                            : "d-block"
                                    }`}
                                >
                                    <span class="label-input100">
                                        Confirm Password
                                    </span>
                                    <input
                                        class="input100"
                                        type="password"
                                        name="pass"
                                        placeholder="Type your password"
                                    />
                                </div>

                                <div class="container-login100-form-btn">
                                    <div class="wrap-login100-form-btn">
                                        <button class="btn btn-primary" type="submit">
                                            {isLoginSession === true
                                                ? "Sign in"
                                                : "Sign up"}
                                        </button>
                                    </div>
                                </div>

                                <div class="txt1 text-center p-t-54 p-b-20">
                                    <span
                                        className="click"
                                        onClick={() => {
                                          //   setIsLoginSession(!isLoginSession);
                                        }}
                                    >
                                        Or{" "}
                                        {isLoginSession === true
                                            ? "Sign up"
                                            : "Sign in"}
                                    </span>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8 banner-sec"></div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    );
};

export default Auth;
