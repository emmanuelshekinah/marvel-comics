import React from "react";

const Footer = (props) => {
    return (
        <footer>
            <div id="sub-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="copyright">
                                <p>
                                    <span>
                                        &copy; Fitness 2018 All right reserved.{" "}
                                    </span>{" "}
                                    <a
                                        href="https://webthemez.com/free-bootstrap-templates/"
                                        target="_blank"
                                    >
                                        Free Bootstrap Template
                                    </a>{" "}
                                    by WebThemez.com
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <ul className="social-network">
                                <li>
                                    <a
                                        className="waves-effect waves-dark"
                                        href="#"
                                        data-placement="top"
                                        title="Facebook"
                                    >
                                        <i className="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a
                                        className="waves-effect waves-dark"
                                        href="#"
                                        data-placement="top"
                                        title="Twitter"
                                    >
                                        <i className="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a
                                        className="waves-effect waves-dark"
                                        href="#"
                                        data-placement="top"
                                        title="Linkedin"
                                    >
                                        <i className="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a
                                        className="waves-effect waves-dark"
                                        href="#"
                                        data-placement="top"
                                        title="Pinterest"
                                    >
                                        <i className="fa fa-pinterest"></i>
                                    </a>
                                </li>
                                <li>
                                    <a
                                        className="waves-effect waves-dark"
                                        href="#"
                                        data-placement="top"
                                        title="Google plus"
                                    >
                                        <i className="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
