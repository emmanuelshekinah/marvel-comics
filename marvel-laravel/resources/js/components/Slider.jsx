import React from "react";

const Slider = (props) => {
    return (
        <section id="banner">
            <div id="main-slider" className="flexslider">
                <ul className="slides">
                    <li>
                        <img src="img/slides/sp.jpeg" alt="" />
                        <div className="flex-caption">
                            {/* <h3>Progress</h3>
                            <p>Enjoy the glow of good health</p> */}
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    );
};

export default Slider;
