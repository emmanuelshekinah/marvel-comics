import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import Footer from "./Footer";
import Header from "./Header";
import Auth from "./pages/auth/Auth";
import Comics from "./pages/Comics";
import Slider from "./Slider";
import {
    BrowserRouter as Router,
    // MemoryRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
} from "react-router-dom";
import CookieService from "./service/CookieService";

function Init() {
    
    useEffect(()=>{
        const cookie = CookieService.get('access_token')
        if(!cookie){
            document.getElementById('login').click()
        }
    }, [])


    return (
        <React.Fragment>
            <Router>
            <Link id="login" to='/auth' ></Link>
                <Switch>
                
               
                    <Route path="/" exact>
                        <div id="wrapper" className="home-page">
                            <Header />
                            <Slider />
                            <Comics />
                            <Footer />
                        </div>
                    </Route>
                    <Route path="/auth" exact>
                        <Auth />
                    </Route>

                    
                </Switch>
            </Router>
        </React.Fragment>
    );
}

export default Init;

if (document.getElementById("root")) {
    ReactDOM.render(<Init />, document.getElementById("root"));
}
