import React from "react";
import CookieService from "./service/CookieService";
import { Link } from "react-router-dom";
const Header = (props) => {
    const logOut = () => {
        CookieService.remove("access_token");
        window.location.reload(true)
    };
    return (
        <header>
            <div className="navbar navbar-default navbar-static-top">
                <div className="container">
                    <div className="navbar-header">
                        
                        <button
                            type="button"
                            className="navbar-toggle"
                            data-toggle="collapse"
                            data-target=".navbar-collapse"
                        >
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="index.html">
                            Marvel Comics
                        </a>
                    </div>
                    <div className="navbar-collapse collapse ">
                        <ul className="nav navbar-nav">
                            <li className="active">
                                <a
                                    className="waves-effect waves-dark"
                                    href="index.html"
                                >
                                    Comics
                                </a>
                            </li>

                            <li>
                                <Link
                                    className="waves-effect waves-dark"
                                    to="/auth"
                                    onClick={()=>{logOut()}}
                                >
                                    LogOut
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;
