<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SyncDataController extends Controller
{
    public function getComics(Request $request){

        $ts = env('MARVEL_TS');
        $apikey = env('MARVEL_PUBLIC_KEY');
        $privateKey = env('MARVEL_PUBLIC_KEY');
        $hash = '806885bc299dc0d1f21129982d9a5a46';//md5($ts.$privateKey.$apikey);
       
        $response = Http::get('https://gateway.marvel.com:443/v1/public/comics?ts='.$ts.'&apikey='.$apikey.'&hash='.$hash);
        
        if($response->status()===200){

            foreach($response->object()->data->results as $data){
                ComicsController::saveComic($data->title, $data->id, $data->description, $data->thumbnail->path.'.'.$data->thumbnail->extension);
            }

            echo "Data Synced";

        }else{
            //something went wrong
           
        }
    }
}
