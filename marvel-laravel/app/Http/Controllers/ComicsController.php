<?php

namespace App\Http\Controllers;
use App\Models\Comics;

use Illuminate\Http\Request;

class ComicsController extends Controller
{
    public static function saveComic($title, $id, $description, $thumbnail){
        $comics = new Comics();
        $comics->_id = $id;
        $comics->title = $title;
        $comics->description = $description;
        $comics->thumbnail = $thumbnail;
        $comics->save();

    }
    
    public function getAllCommics(Request $request){

        $searchInput = $request->searchInput;

        $res= Comics::query()
        ->where('title', 'LIKE', "%{$searchInput}%")
        ->get();

        return array(
            'data'=>$res
        );
    }
}
