
## Commands
docker-compose up -d
docker-compose exec app /bin/bash
composer install
php artisan key:generate
php artisan passport:install
exit

## Sync Data
visit http://baseUrl/syncdata
